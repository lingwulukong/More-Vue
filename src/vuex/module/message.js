import {
    INC,
    DEC,
} from '../types.js'

const state = {
    cnt: 0
}

const getters = {
    cnt: state => state.cnt
}

const mutations = {
    [INC](state) {
        state.cnt += 1
    }, 
    [DEC](state) {
        state.cnt -= 1
    }
}

export default {
    state,
    getters,
    mutations
}