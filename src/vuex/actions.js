import * as types from './types'

export const addCnt = ({ commit }) => {
    commit(types.INC)
}

export const minusCnt = ({ commit }) => {
    commit(types.DEC)
}

