import Vue from 'vue'
import Vuex from 'vuex'

import button from './module/button'
import message from './module/message'

import * as actions from './actions'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    actions,
    modules: {
      message
    },
    strict: debug,
})